<?php 	
	$firstname = trim($_POST['firstname']);
	$lastname = trim($_POST['lastname']);
	$email = trim($_POST['email']);
	$phone = trim($_POST['phone']);
	
	if(function_exists('stripslashes')) {
		$message = stripslashes(trim($_POST['message']));
	} else {
		$message = trim($_POST['message']);
	}
		
	$emailTo = 'contact@patheoslabs.com';
	$subject = 'Hello, Patheos Labs — from '.$firstname;
	$sendCopy = trim($_POST['sendCopy']);
	$body = "Name: $firstname $lastname \n\nEmail: $email \n\nPhone: $phone \n\nMessage: $message";
	$headers = 'From: Contact Form at learn.patheoslabs.com <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
	
	mail($emailTo, $subject, $body, $headers);
	
	return true;
?>